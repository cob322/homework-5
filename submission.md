# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

    ((λp.pz) (λq.(w (λw.((wqz)p)))))


2. λp.pq λp.qp
    
    (λp.((p)q (λp.((q)p))))
    


## Question 2

!["Question 2"](2.jpg)

## Question 3 & 4

!["Question 3 P1"](3_1.jpg)
!["Question 3 P2"](3_2.jpg)
!["Question 3 and some 4"](3_3+4_1.jpg)
!["Question 4 finish"](4_2.jpg)




## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

!["Question 5"](5.jpg)

Similiar to an if statement, the not operator takes in a boolean (symbolized by y), and will then return one of the two functions which make it up (symbolized by the two functions noted by "negate false"). I thought of this by thinking of Racket syntax where a condition is taken in and then one of the two expressions passed to it are returned to the user.




## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.